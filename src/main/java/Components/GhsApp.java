package Components;

import Ghs.Components.Node;
import Ghs.Events.InitMessage;
import Ghs.Ports.EdgePort;
import se.sics.kompics.Channel;
import se.sics.kompics.Component;
import se.sics.kompics.ComponentDefinition;
import se.sics.kompics.Kompics;
import utils.Edge;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class GhsApp extends ComponentDefinition {

    private String startNode = "";
    private ArrayList<Edge> edges = new ArrayList<>();
    private Map<String, Component> components = new HashMap<>();

    public GhsApp() {
        readTable("src/main/java/tables.txt");
    }

    public static void main(String[] args) throws InterruptedException {
        Kompics.createAndStart(GhsApp.class);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            System.exit(10);
        }
        Kompics.shutdown();
        Kompics.waitForTermination();
    }

    private void readTable(String inputFile) {
        File resourceFile = new File(inputFile);
        try (Scanner scanner = new Scanner(resourceFile)) {
            int i = 0;
            while (scanner.hasNext()) {
                String line = scanner.nextLine();

                if (i > 0) {
                    if (line.split(",").length > 1) {
                        int weight = Integer.parseInt(line.split(",")[1]);
                        String rel = line.split(",")[0];
                        String src = rel.split("-")[0];
                        String dst = rel.split("-")[1];
                        edges.add(new Edge(src, dst, weight));
                    } else {
                        startNode = line;
                        for (Edge edge : edges) {
                            if (!components.containsKey(edge.src)) {
                                Component c = create(Node.class, new InitMessage(edge.src, edge.src.equalsIgnoreCase
                                        (startNode), findNeighbours(edge.src)));
                                components.put(edge.src, c);
                            }
                            if (!components.containsKey(edge.dst)) {
                                Component c = create(Node.class, new InitMessage(edge.dst, edge.dst.equalsIgnoreCase
                                        (startNode), findNeighbours(edge.dst)));
                                components.put(edge.dst, c);
                            }
                            connect(components.get(edge.src).getPositive(EdgePort.class),
                                    components.get(edge.dst).getNegative(EdgePort.class), Channel.TWO_WAY);
                            connect(components.get(edge.src).getNegative(EdgePort.class),
                                    components.get(edge.dst).getPositive(EdgePort.class), Channel.TWO_WAY);
                        }
                    }
                }
                i++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private HashMap<String, Integer> findNeighbours(String node) {
        HashMap<String, Integer> nb = new HashMap<String, Integer>();
        for (Edge tr : edges) {
            if (tr.src.equalsIgnoreCase(node) && !nb.containsKey(tr.dst)) {
                nb.put(tr.dst, tr.weight);
            } else if (tr.dst.equalsIgnoreCase(node) && !nb.containsKey(tr.src)) {
                nb.put(tr.src, tr.weight);
            }
        }
        return nb;
    }
}
