package Components;

import MapReduce.Components.Node;
import MapReduce.Events.InitMessage;
import MapReduce.Ports.EdgePort;
import se.sics.kompics.Channel;
import se.sics.kompics.Component;
import se.sics.kompics.ComponentDefinition;
import se.sics.kompics.Kompics;
import utils.Edge;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class MapReduceApp extends ComponentDefinition {

    private String startNode = "";
    private ArrayList<Edge> edges = new ArrayList<>();
    private Map<String, Component> components = new HashMap<>();
    private int numOfnodes;

    public MapReduceApp() {
        readTable("src/main/java/mst.txt");
    }

    public static void main(String[] args) throws InterruptedException {
        Kompics.createAndStart(MapReduceApp.class);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            System.exit(10);
        }
        Kompics.shutdown();
        Kompics.waitForTermination();
    }

    private void readTable(String inputFile) {
        File resourceFile = new File(inputFile);
        try (Scanner scanner = new Scanner(resourceFile)) {
            int i = 0;
            while (scanner.hasNext()) {
                String line = scanner.nextLine();

                if (i > 0) {
                    if (line.split(",").length > 1) {
                        int weight = Integer.parseInt(line.split(",")[1]);
                        String rel = line.split(",")[0];
                        String src = rel.split("-")[0];
                        String dst = rel.split("-")[1];
                        edges.add(new Edge(src, dst, weight));
                    } else {
                        startNode = line;
                        List<String> nodes = new ArrayList<>();
                        for (Edge edge : edges) {
                            if (!nodes.contains(edge.src))
                                nodes.add(edge.src);
                            if (!nodes.contains(edge.dst))
                                nodes.add(edge.dst);
                        }

                        for (Edge edge : edges) {
                            if (!components.containsKey(edge.src)) {
                                Component c = create(Node.class, new InitMessage(edge.src,
                                        findNeighbours(edge.src),
                                        nodes));
                                components.put(edge.src, c);
                            }
                            if (!components.containsKey(edge.dst)) {
                                Component c = create(Node.class, new InitMessage(edge.dst,
                                        findNeighbours(edge.dst),
                                        nodes));
                                components.put(edge.dst, c);
                            }
                            connect(components.get(edge.src).getPositive(EdgePort.class),
                                    components.get(edge.dst).getNegative(EdgePort.class), Channel.TWO_WAY);
                            connect(components.get(edge.src).getNegative(EdgePort.class),
                                    components.get(edge.dst).getPositive(EdgePort.class), Channel.TWO_WAY);
                        }
                    }
                }
                i++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private HashMap<String, Integer> findNeighbours(String node) {
        HashMap<String, Integer> nb = new HashMap<>();
        for (Edge tr : edges) {
            if (tr.src.equalsIgnoreCase(node) && !nb.containsKey(tr.dst)) {
                nb.put(tr.dst, tr.weight);
            } else if (tr.dst.equalsIgnoreCase(node) && !nb.containsKey(tr.src)) {
                nb.put(tr.src, tr.weight);
            }
        }
        return nb;
    }

    private static List<String> nodes = new ArrayList<>();
    private static List<Edge> importedEdges = new ArrayList<>();

    public static void addEdge(String p, String q, int weight) {
        if (!nodes.contains(p))
            nodes.add(p);
        if (!nodes.contains(q))
            nodes.add(q);

        Edge firstEdgeCandidate = new Edge(p, q, weight);
        Edge secondEdgeCandidate = new Edge(q, p, weight);

        if (!importedEdges.contains(firstEdgeCandidate) &&
                !importedEdges.contains(secondEdgeCandidate)) {
            System.out.println(p + "-" + q + "," + weight);
            importedEdges.add(firstEdgeCandidate);
        }
    }
}
