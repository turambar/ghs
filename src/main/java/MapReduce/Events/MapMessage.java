package MapReduce.Events;

import se.sics.kompics.KompicsEvent;

public class MapMessage implements KompicsEvent {
    public String src;
    public String dst;
    public int startId;
    public int totalLeaves;
    public String filename;

    public MapMessage(String src, String dst, int startId, int totalLeaves, String filename) {
        this.src = src;
        this.dst = dst;
        this.startId = startId;
        this.totalLeaves = totalLeaves;
        this.filename = filename;
    }
}
