package MapReduce.Events;

import se.sics.kompics.KompicsEvent;

public class LeaderBroadcastMessage implements KompicsEvent {
    public String src;
    public String dst;
    public String leader;
    public int avgDistance;

    public LeaderBroadcastMessage(String src, String dst, String leader, int avgDistance) {
        this.src = src;
        this.dst = dst;
        this.leader = leader;
        this.avgDistance = avgDistance;
    }
}
