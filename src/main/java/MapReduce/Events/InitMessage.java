package MapReduce.Events;

import MapReduce.Components.Node;
import se.sics.kompics.Init;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class InitMessage extends Init<Node> {
    public String nodeName;
    public HashMap<String, Integer> neighbours;
    public int numOfNodes;
    public List<String> treeNodes;

    public InitMessage(String nodeName, HashMap<String, Integer> neighbours, List<String> treeNodes) {
        this.nodeName = nodeName;
        this.neighbours = neighbours;
        this.treeNodes = treeNodes;
    }
}
