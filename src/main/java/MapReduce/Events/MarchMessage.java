package MapReduce.Events;

import se.sics.kompics.KompicsEvent;

public class MarchMessage implements KompicsEvent {
    public String src;
    public String dst;

    public MarchMessage(String src, String dst) {
        this.src = src;
        this.dst = dst;
    }
}
