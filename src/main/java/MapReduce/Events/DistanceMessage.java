package MapReduce.Events;

import se.sics.kompics.KompicsEvent;

import java.util.HashMap;

public class DistanceMessage implements KompicsEvent {
    public String src;
    public String dst;
    public String broadcaster;
    public HashMap<String, Integer> distances;

    public DistanceMessage(String src, String dst, String broadcaster, HashMap<String, Integer> distances) {
        this.src = src;
        this.dst = dst;
        this.broadcaster = broadcaster;
        this.distances = distances;
    }
}
