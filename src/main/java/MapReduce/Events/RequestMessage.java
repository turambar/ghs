package MapReduce.Events;

import se.sics.kompics.KompicsEvent;

public class RequestMessage implements KompicsEvent {
    public String src;
    public String dst;
    public int round;

    public RequestMessage(String src, String dst, int round) {
        this.src = src;
        this.dst = dst;
        this.round = round;
    }
}
