package MapReduce.Events;

import se.sics.kompics.KompicsEvent;

import java.util.HashMap;

public class ReduceMessage implements KompicsEvent {
    public String src;
    public String dst;
    public HashMap<String, Integer> data;

    public ReduceMessage(String src, String dst, HashMap<String, Integer> data) {
        this.src = src;
        this.dst = dst;
        this.data = data;
    }
}
