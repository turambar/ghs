package MapReduce.Events;

import se.sics.kompics.KompicsEvent;

public class MarchAckMessage implements KompicsEvent {
    public String src;
    public String dst;
    public int leaves;

    public MarchAckMessage(String src, String dst, int leaves) {
        this.src = src;
        this.dst = dst;
        this.leaves = leaves;
    }
}
