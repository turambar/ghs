package MapReduce.Events;

import se.sics.kompics.KompicsEvent;

public class LeaderElectionAck implements KompicsEvent {
    public String src;
    public String dst;
    public String electedNode;

    public LeaderElectionAck(String src, String dst, String electedNode) {
        this.src = src;
        this.dst = dst;
        this.electedNode = electedNode;
    }
}
