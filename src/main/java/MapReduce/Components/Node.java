package MapReduce.Components;

import MapReduce.Events.*;
import MapReduce.Ports.EdgePort;
import se.sics.kompics.*;

import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.util.*;
import java.util.List;

public class Node extends ComponentDefinition {
    private Negative<EdgePort> sendPort = negative(EdgePort.class);
    private Positive<EdgePort> recievePort = positive(EdgePort.class);

    private String nodeName;
    private HashMap<String, Integer> neighbours;
    private List<String> treeNodes;
    private int numOfnodes;
    private HashMap<String, Integer> distances;
    private Set<String> receivedBroadcasts;
    private boolean routingTerminated;
    private int avgDistance;
    private HashMap<String, Integer> electedRequests;
    private HashMap<String, String> electedParents;
    private String electedParent;
    private static final int MAX_DIST = 1000;

    private HashMap<String, Integer> electedNodeAcks;

    private boolean isLeaderElected;
    private boolean isLeader;
    private boolean isLeaf;

    private HashMap<String, Integer> bottomLeaves;

    private int marchCounter;
    private String parent;
    private int reduceCount;
    private HashMap<String, Integer> reductionData;

    private HashMap<String, Integer> mapFunction(MapMessage mapMessage) {
        String data;
        try {
            File file = new File(mapMessage.filename);
            FileInputStream fis = null;
            fis = new FileInputStream(file);
            byte[] dataByte = new byte[(int) file.length()];
            fis.read(dataByte);
            fis.close();
            data = new String(dataByte, "UTF-8");
        } catch (Exception ignored) {
            System.out.println("exception");
            return null;
        }

        String[] splitData = data.trim().split("\\s+");
        int splitDataSize = splitData.length;

        HashMap<String, Integer> mapData = new HashMap<>();
        int mapUnit = splitDataSize / mapMessage.totalLeaves;

        for (int i = mapUnit * mapMessage.startId; i < (mapUnit * (mapMessage.startId + 1)); i++) {
            if (mapData.containsKey(splitData[i]))
                mapData.replace(splitData[i], mapData.get(splitData[i]) + 1);
            else
                mapData.put(splitData[i], 1);
        }

        return mapData;
    }

    private Handler<ReduceMessage> reduceMessageHandler = new Handler<ReduceMessage>() {
        @Override
        public void handle(ReduceMessage event) {
            if (!event.dst.equals(nodeName))
                return;

            for (Map.Entry<String, Integer> entry : event.data.entrySet())
                if (reductionData.containsKey(entry.getKey()))
                    reductionData.replace(entry.getKey(), reductionData.get(entry.getKey()) + entry.getValue());
                else
                    reductionData.put(entry.getKey(), entry.getValue());

            reduceCount++;
            if (isLeader) {
                if (reduceCount == neighbours.size()) {
                    System.out.println("reduction complete");
                    for (Map.Entry<String, Integer> entry : reductionData.entrySet())
                        System.out.println(entry.getKey() + " : " + entry.getValue());
                }
                return;
            }

            if (reduceCount == (neighbours.size() - 1))
                trigger(new ReduceMessage(nodeName, parent, reductionData), sendPort);
        }
    };

    private Handler<MapMessage> mapMessageHandler = new Handler<MapMessage>() {
        @Override
        public void handle(MapMessage event) {
            if (!event.dst.equals(nodeName))
                return;

            if (isLeaf) {
                HashMap<String, Integer> extractedData = new HashMap<>();
                extractedData = mapFunction(event);
                trigger(new ReduceMessage(nodeName, parent, extractedData), sendPort);
            }

            reduceCount = 0;
            reductionData = new HashMap<>();
            int startId = event.startId;
            for (Map.Entry<String, Integer> subTree : bottomLeaves.entrySet()) {
                trigger(new MapMessage(nodeName, subTree.getKey(), startId, event.totalLeaves, event.filename), sendPort);
                startId += subTree.getValue();
            }
        }
    };

    private void distributeMap() {
        int numOfleaves = bottomLeaves.values().stream().mapToInt(Integer::intValue).sum();

        reductionData = new HashMap<>();
        reduceCount = 0;

        int startId = 0;
        for (Map.Entry<String, Integer> subTree : bottomLeaves.entrySet()) {
            trigger(new MapMessage(nodeName, subTree.getKey(), startId, numOfleaves, "src/main/java/test.txt"), sendPort);
            startId += subTree.getValue();
        }
    }

    private Handler<MarchAckMessage> marchAckHandler = new Handler<MarchAckMessage>() {
        @Override
        public void handle(MarchAckMessage event) {
            if (!event.dst.equals(nodeName))
                return;

            marchCounter++;
            bottomLeaves.put(event.src, event.leaves);

            if (isLeader) {
                if (marchCounter == neighbours.size()) {
                    System.out.println(nodeName + " say march completed with leaves : " + bottomLeaves);
                    distributeMap();
                    return;
                }
                return;
            }

            if (marchCounter == (neighbours.size() - 1))
                trigger(new MarchAckMessage(nodeName, parent, bottomLeaves.size()), sendPort);
        }
    };

    private Handler<MarchMessage> marchHandler = new Handler<MarchMessage>() {
        @Override
        public void handle(MarchMessage event) {
            if (!event.dst.equals(nodeName))
                return;

            isLeaderElected = true;
            parent = event.src;
            marchCounter = 0;
            bottomLeaves = new HashMap<>();

            if (isLeaf)
                trigger(new MarchAckMessage(nodeName, parent, 1), sendPort);

            for (String neighbour : neighbours.keySet())
                if (!neighbour.equals(parent))
                    trigger(new MarchMessage(nodeName, neighbour), sendPort);
        }
    };

    private void marchAllTheNodes() {
        marchCounter = 0;
        bottomLeaves = new HashMap<>();

        for (String neighbour : neighbours.keySet())
            trigger(new MarchMessage(nodeName, neighbour), sendPort);
    }

    private Handler<LeaderElectionAck> leaderElectionAckHandler = new Handler<LeaderElectionAck>() {
        @Override
        public void handle(LeaderElectionAck event) {
            if (!event.dst.equals(nodeName))
                return;

            if (!electedNodeAcks.containsKey(event.electedNode))
                electedNodeAcks.put(event.electedNode, 0);

            electedNodeAcks.replace(event.electedNode, electedNodeAcks.get(event.electedNode) + 1);

            if (event.electedNode.equals(nodeName) &&
                    (electedNodeAcks.get(event.electedNode) == neighbours.size())) {
                System.out.println("node " + nodeName + " elected ");
                isLeader = true;
                marchAllTheNodes();
                return;
            }

            if (nodeName.equals(event.electedNode))
                return;

            if (electedNodeAcks.get(event.electedNode) == (neighbours.size() - 1))
                trigger(new LeaderElectionAck(nodeName, electedParents.get(event.electedNode), event.electedNode), sendPort);

        }
    };

    private Handler<LeaderBroadcastMessage> leaderBroadcastHandler = new Handler<LeaderBroadcastMessage>() {
        @Override
        public void handle(LeaderBroadcastMessage event) {
            if (!event.dst.equals(nodeName))
                return;

            if (isLeaderElected)
                return;

            if (!routingTerminated) {
                electedRequests.put(event.leader, event.avgDistance);
                electedParents.put(event.leader, event.src);
                return;
            }

            if (event.avgDistance > avgDistance ||
                    ((event.avgDistance == avgDistance) && (event.leader.compareTo(nodeName) > 0)))
                return;

            if (neighbours.size() == 1) {
                trigger(new LeaderElectionAck(nodeName, event.src, event.leader), sendPort);
                return;
            }

            electedParents.put(event.leader, event.src);
            for (String neighbour : neighbours.keySet())
                if (!neighbour.equals(event.src))
                    trigger(new LeaderBroadcastMessage(nodeName, neighbour, event.leader, event.avgDistance), sendPort);
        }
    };

    private Handler<DistanceMessage> distanceHandler = new Handler<DistanceMessage>() {
        @Override
        public void handle(DistanceMessage event) {
            if (!event.dst.equals(nodeName))
                return;

            if (routingTerminated)
                return;

            if (isLeaderElected)
                return;

            HashMap<String, Integer> newDistances = new HashMap<>();
            for (Map.Entry<String, Integer> receivedDistance : event.distances.entrySet())
                newDistances.put(receivedDistance.getKey(), receivedDistance.getValue());

            for (Map.Entry<String, Integer> receivedDistance : newDistances.entrySet()) {
                if (distances.get(receivedDistance.getKey()) < (receivedDistance.getValue() + neighbours.get(event.src))) {
                    newDistances.replace(receivedDistance.getKey(), distances.get(receivedDistance.getKey()));
                } else {
                    distances.replace(receivedDistance.getKey(), receivedDistance.getValue() + neighbours.get(event.src));
                    newDistances.replace(receivedDistance.getKey(), receivedDistance.getValue() + neighbours.get(event.src));
                }
            }

            receivedBroadcasts.add(event.broadcaster);

            for (Map.Entry<String, Integer> entry : neighbours.entrySet())
                trigger(new DistanceMessage(nodeName, entry.getKey(), event.broadcaster, newDistances), sendPort);

            boolean receivedAllBroadCasters = true;
            for (String node : treeNodes)
                if (!receivedBroadcasts.contains(node)) {
                    receivedAllBroadCasters = false;
                    break;
                }


            if (receivedAllBroadCasters) {
                routingTerminated = true;

                avgDistance = distances.values().stream().mapToInt(Integer::intValue).sum() / numOfnodes;

                System.out.println(nodeName + " terminated : " + distances + " avg distance : " + avgDistance);

                Map.Entry<String, Integer> minEntry = null;
                for (Map.Entry<String, Integer> entry : electedRequests.entrySet()) {
                    if (minEntry == null || minEntry.getValue() > entry.getValue()) {
                        minEntry = entry;
                    }
                }

                String electedNode;
                if (minEntry == null || avgDistance < minEntry.getValue()) {
                    electedNode = nodeName;
                    electedParent = null;
                } else {
                    electedNode = minEntry.getKey();
                    avgDistance = minEntry.getValue();
                    electedParent = electedParents.get(minEntry.getKey());
                }

                for (String neighbour : neighbours.keySet())
                    if (!neighbour.equals(electedParent))
                        trigger(new LeaderBroadcastMessage(nodeName, neighbour, electedNode, avgDistance), sendPort);

            }
        }
    };

    private Handler<Start> startHandler = new Handler<Start>() {
        @Override
        public void handle(Start event) {
            if (isLeaderElected)
                return;

            HashMap<String, Integer> distanceMessage = new HashMap<>();

            for (Map.Entry<String, Integer> entry : distances.entrySet())
                distanceMessage.put(entry.getKey(), entry.getValue());

            for (String neighbour : neighbours.keySet())
                trigger(new DistanceMessage(nodeName, neighbour, nodeName, distanceMessage), sendPort);
        }
    };

    public Node(InitMessage initMessage) {
        nodeName = initMessage.nodeName;
        neighbours = initMessage.neighbours;
        numOfnodes = initMessage.treeNodes.size();
        treeNodes = initMessage.treeNodes;
        routingTerminated = false;

        receivedBroadcasts = new HashSet<>();
        receivedBroadcasts.add(nodeName);
        distances = new HashMap<>();
        for (String treeNode : treeNodes)
            distances.put(treeNode, neighbours.getOrDefault(treeNode, MAX_DIST));
        distances.replace(nodeName, 0);

        System.out.println(nodeName + " : " + distances);

        electedRequests = new HashMap<>();
        electedParents = new HashMap<>();
        electedNodeAcks = new HashMap<>();

        isLeaderElected = false;
        isLeader = false;
        isLeaf = (neighbours.size() == 1);
        marchCounter = 0;

        subscribe(startHandler, control);
        subscribe(distanceHandler, recievePort);
        subscribe(leaderBroadcastHandler, recievePort);
        subscribe(leaderElectionAckHandler, recievePort);
        subscribe(marchHandler, recievePort);
        subscribe(marchAckHandler, recievePort);
        subscribe(mapMessageHandler, recievePort);
        subscribe(reduceMessageHandler, recievePort);
    }
}

