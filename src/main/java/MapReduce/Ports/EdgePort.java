package MapReduce.Ports;

import MapReduce.Events.*;
import se.sics.kompics.PortType;

public class EdgePort extends PortType {
    {
        positive(MapMessage.class);
        negative(MapMessage.class);

        positive(DistanceMessage.class);
        negative(DistanceMessage.class);

        positive(LeaderBroadcastMessage.class);
        negative(LeaderBroadcastMessage.class);

        positive(LeaderElectionAck.class);
        negative(LeaderElectionAck.class);

        positive(MarchMessage.class);
        negative(MarchMessage.class);

        positive(MarchAckMessage.class);
        negative(MarchAckMessage.class);

        positive(ReduceMessage.class);
        negative(ReduceMessage.class);
    }
}
