package Ghs.Events;

import se.sics.kompics.KompicsEvent;

public class TestMessage implements KompicsEvent {
    public String src;
    public String dst;
    public int pName;
    public int level;

    public TestMessage(String src, String dst, int pName, int level) {
        this.src = src;
        this.dst = dst;
        this.pName = pName;
        this.level = level;
    }
}
