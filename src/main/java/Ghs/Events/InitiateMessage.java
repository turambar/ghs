package Ghs.Events;

import Ghs.NodeState;
import se.sics.kompics.KompicsEvent;

public class InitiateMessage implements KompicsEvent {
    public String src;
    public String dst;
    public String nodeName;
    public int pName;
    public int level;
    public NodeState nodeState;

    public InitiateMessage(String src, String dst, String nodeName, int pName, int level, NodeState nodeState) {
        this.src = src;
        this.dst = dst;
        this.nodeName = nodeName;
        this.pName = pName;
        this.level = level;
        this.nodeState = nodeState;
    }
}
