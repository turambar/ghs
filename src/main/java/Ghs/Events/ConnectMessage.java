package Ghs.Events;

import se.sics.kompics.KompicsEvent;

public class ConnectMessage implements KompicsEvent {
    public String src;
    public String dst;
    public int level;

    public ConnectMessage(String src, String dst, int level) {
        this.src = src;
        this.dst = dst;
        this.level = level;
    }
}
