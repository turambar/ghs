package Ghs.Events;

import se.sics.kompics.KompicsEvent;

public class ReportMessage implements KompicsEvent {
    public String dst;
    public String src;
    public int bestWeight;

    public ReportMessage(String src, String dst, int bestWeight) {
        this.dst = dst;
        this.src = src;
        this.bestWeight = bestWeight;
    }
}
