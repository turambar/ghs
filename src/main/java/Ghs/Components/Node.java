package Ghs.Components;

import Components.MapReduceApp;
import Ghs.NodeState;
import Ghs.Events.*;
import Ghs.Ports.EdgePort;
import se.sics.kompics.*;

import java.util.HashMap;
import java.util.Map;

public class Node extends ComponentDefinition {
    private Negative<EdgePort> sendPort = negative(EdgePort.class);

    static class TestData {
        int pName;
        int level;

        TestData(int pName, int level) {
            this.pName = pName;
            this.level = level;
        }
    }

    enum EdgeStatus {
        BASIC,
        BRANCH,
        REJECTED,
    }

    private String nodeName;
    private int pName;
    private HashMap<String, Integer> neighbours;

    private int level;
    private int counter;

    private int bestWeight;
    private int parentReport;

    private String parent;
    private String bestEdge;
    private String testEdge;

    private NodeState nodeState;

    private HashMap<String, Integer> connectionIssuers;
    private HashMap<String, EdgeStatus> edgeStates;
    private HashMap<String, TestData> testers;

    private void findMinimalOutgoingEdge() {
        testEdge = null;
        int minEdge = Integer.MAX_VALUE;

        for (Map.Entry<String, Integer> entry : neighbours.entrySet()) {
            if (entry.getValue() < minEdge && edgeStates.get(entry.getKey()).equals(EdgeStatus.BASIC)) {
                testEdge = entry.getKey();
                minEdge = entry.getValue();
            }
        }

        if (testEdge != null) {
            trigger(new TestMessage(nodeName, testEdge, pName, level), sendPort);
        } else if (counter == numOfBranchEdges()) {
                sendReport();
        }
    }

    private void replyTest(String adjacentNode, int fn) {
        if (pName != fn) {
            trigger(new AcceptMessage(nodeName, adjacentNode), sendPort);
        } else {
            edgeStates.replace(adjacentNode, EdgeStatus.REJECTED);
            if (!testEdge.equals(adjacentNode)) {
                trigger(new RejectMessage(nodeName, adjacentNode), sendPort);
            } else {
                findMinimalOutgoingEdge();
            }
        }
    }

    private void sendReport() {
        nodeState = NodeState.FOUND;
        trigger(new ReportMessage(nodeName, parent, bestWeight), sendPort);

        if (parentReport > 0 && bestWeight < parentReport) {
            changeRoot();
        }
    }

    private int numOfBranchEdges() {
        int branchEdges = 0;
        for (Map.Entry<String, EdgeStatus> entry : edgeStates.entrySet()) {
            if (entry.getValue() == EdgeStatus.BRANCH)
                branchEdges++;
        }

        return branchEdges;
    }

    private void changeRoot() {
        if (edgeStates.get(bestEdge) == EdgeStatus.BRANCH) {
            trigger(new ChangeRootMessage(nodeName, bestEdge), sendPort);
        } else {
            edgeStates.replace(bestEdge, EdgeStatus.BRANCH);
            MapReduceApp.addEdge(nodeName, bestEdge, neighbours.get(bestEdge));
            trigger(new ConnectMessage(nodeName, bestEdge, level), sendPort);

            if (connectionIssuers.get(bestEdge) != null &&
                    connectionIssuers.get(bestEdge) == level) {
                trigger(new InitiateMessage(nodeName, bestEdge, nodeName, bestWeight, level + 1, NodeState.FIND), sendPort);
                connectionIssuers.remove(bestEdge, level);
            }
        }
    }

    public Node(InitMessage initMessage) {
        nodeName = initMessage.nodeName;
        neighbours = new HashMap<>();
        edgeStates = new HashMap<>();
        connectionIssuers = new HashMap<>();
        testers = new HashMap<>();

        counter = 0;
        level = 0;
        parentReport = 0;

        neighbours = initMessage.neighbours;
        for (Map.Entry<String, Integer> entry : neighbours.entrySet()) {
            edgeStates.put(entry.getKey(), EdgeStatus.BASIC);
        }

        Positive<EdgePort> recievePort = positive(EdgePort.class);

        Handler<Start> startHandler = new Handler<Start>() {
            @Override
            public void handle(Start event) {
                int minEdge = Integer.MAX_VALUE;
                String nearestNode = null;

                for (Map.Entry<String, Integer> entry : neighbours.entrySet()) {
                    if (entry.getValue() < minEdge) {
                        minEdge = entry.getValue();
                        nearestNode = entry.getKey();
                    }
                }
                if (nearestNode == null)
                    return;

                edgeStates.replace(nearestNode, EdgeStatus.BRANCH);
                MapReduceApp.addEdge(nodeName, nearestNode, neighbours.get(nearestNode));

                counter = 1;
                parentReport = 0;
                nodeState = NodeState.FOUND;

                trigger(new ConnectMessage(nodeName, nearestNode, level), sendPort);
            }
        };

        subscribe(startHandler, control);

        Handler<ConnectMessage> connectMessageHandler = new Handler<ConnectMessage>() {
            @Override
            public void handle(ConnectMessage event) {
                if (!event.dst.equals(nodeName))
                    return;

                if (event.level < level) {
                    trigger(new InitiateMessage(nodeName, event.src, nodeName, pName, level, nodeState), sendPort);
                    edgeStates.replace(event.src, EdgeStatus.BRANCH);
                    MapReduceApp.addEdge(nodeName, event.src, neighbours.get(event.src));
                } else if (edgeStates.get(event.src).equals(EdgeStatus.BRANCH)) {
                    trigger(new InitiateMessage(nodeName, event.src, nodeName, neighbours.get(event.src), level + 1, NodeState.FIND), sendPort);
                } else {
                    connectionIssuers.computeIfAbsent(event.src, k -> event.level);
                }
            }
        };

        subscribe(connectMessageHandler, recievePort);

        Handler<TestMessage> testMessageHandler = new Handler<TestMessage>() {
            @Override
            public void handle(TestMessage event) {
                if (!event.dst.equals(nodeName))
                    return;

                if (event.level <= level)
                    replyTest(event.src, event.pName);
                else
                    testers.computeIfAbsent(event.src, k -> new TestData(event.pName, event.level));
            }
        };

        subscribe(testMessageHandler, recievePort);

        Handler<InitiateMessage> initiateMessageHandler = new Handler<InitiateMessage>() {
            @Override
            public void handle(InitiateMessage event) {
                if (!event.dst.equals(nodeName))
                    return;

                pName = event.pName;
                level = event.level;
                nodeState = event.nodeState;
                parent = event.src;
                bestEdge = null;
                bestWeight = Integer.MAX_VALUE;
                counter = 1;
                parentReport = 0;

                for (Map.Entry<String, Integer> entry : connectionIssuers.entrySet()) {
                    if (entry.getValue() < level) {
                        edgeStates.replace(entry.getKey(), EdgeStatus.BRANCH);
                        MapReduceApp.addEdge(nodeName, entry.getKey(), neighbours.get(entry.getKey()));
                        connectionIssuers.remove(entry.getKey());
                    }
                }

                for (Map.Entry<String, Integer> entry : neighbours.entrySet()) {
                    if (!entry.getKey().equals(event.src) &&
                            edgeStates.get(entry.getKey()).equals(EdgeStatus.BRANCH)) {
                        trigger(new InitiateMessage(nodeName, entry.getKey(), event.nodeName, event.pName, event.level, nodeState), sendPort);
                        break;
                    }
                }

                for (Map.Entry<String, TestData> entry : testers.entrySet()) {
                    if (entry.getValue().level <= level) {
                        replyTest(entry.getKey(), entry.getValue().pName);
                        testers.remove(entry.getKey());
                    }
                }

                if (event.nodeState == NodeState.FIND) {
                    findMinimalOutgoingEdge();
                }

            }
        };

        subscribe(initiateMessageHandler, recievePort);

        Handler<ChangeRootMessage> changeRootMessageHandler = new Handler<ChangeRootMessage>() {
            @Override
            public void handle(ChangeRootMessage event) {
                if (!event.dst.equals(nodeName))
                    return;

                changeRoot();
            }
        };

        subscribe(changeRootMessageHandler, recievePort);

        Handler<ReportMessage> reportMessageHandler = new Handler<ReportMessage>() {
            @Override
            public void handle(ReportMessage event) {
                if (!event.dst.equals(nodeName))
                    return;

                if (!event.src.equals(parent)) {
                    counter++;

                    if (event.bestWeight < bestWeight) {
                        bestEdge = event.src;
                        bestWeight = event.bestWeight;
                    }

                    if (counter == numOfBranchEdges() && testEdge == null)
                        sendReport();
                } else if (nodeState == NodeState.FIND) {
                    parentReport = event.bestWeight;
                } else {
                    if (bestWeight < event.bestWeight) {
                        changeRoot();
                    }
                }
            }
        };

        subscribe(reportMessageHandler, recievePort);

        Handler<RejectMessage> rejectMessageHandler = new Handler<RejectMessage>() {
            @Override
            public void handle(RejectMessage event) {
                if (!event.dst.equals(nodeName))
                    return;

                edgeStates.replace(event.src, EdgeStatus.REJECTED);
                findMinimalOutgoingEdge();
            }
        };

        subscribe(rejectMessageHandler, recievePort);

        Handler<AcceptMessage> acceptMessageHandler = new Handler<AcceptMessage>() {
            @Override
            public void handle(AcceptMessage event) {
                if (!event.dst.equals(nodeName))
                    return;

                testEdge = null;

                if (neighbours.get(event.src) < bestWeight) {
                    bestEdge = event.src;
                    bestWeight = neighbours.get(event.src);
                }

                if (counter == numOfBranchEdges())
                    sendReport();
            }
        };

        subscribe(acceptMessageHandler, recievePort);
    }
}

