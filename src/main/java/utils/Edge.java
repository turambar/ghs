package utils;

public class Edge {
    public String src;
    public String dst;
    public int weight;

    public Edge(String src, String dst, int weight) {
        this.src = src;
        this.dst = dst;
        this.weight = weight;
    }

    @Override
    public boolean equals(Object obj) {
        return (this.src.equals(((Edge) obj).src) &&
                this.dst.equals(((Edge) obj).dst) &&
                this.weight == ((Edge) obj).weight);
    }
}
